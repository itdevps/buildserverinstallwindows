function Get-PackageParameters {
    [CmdletBinding()]
    param(
       [string] $Parameters = $Env:ChocolateyPackageParameters,
       # Allows splatting with arguments that do not apply and future expansion. Do not use directly.
       [parameter(ValueFromRemainingArguments = $true)]
       [Object[]] $IgnoredArguments
    )

    $res = @{}
    
    $re = "\/([a-zA-Z0-9]+)(:[`"'].+?[`"']|[^ ]+)?"
    $results = $Parameters | Select-String $re -AllMatches | select -Expand Matches
    
    foreach ($m in $results) {
        if (!$m) { continue } # must because of posh 2.0 bug: https://github.com/chocolatey/chocolatey-coreteampackages/issues/465

        $a = $m.Value -split ':'
        $opt = $a[0].Substring(1); $val = $a[1..100] -join ':'
        if ($val -match '^(".+")|(''.+'')$') {$val = $val -replace '^.|.$'}
        $res[ $opt ] = if ($val) { $val } else { $true }
    }
    $res
}

    try{
        # Now we can use the $env:chocolateyPackageParameters to extract parameters passed to choco install
        $pp = Get-PackageParameters
        $DeployFile = "Install-Docker.ps1"
	    write-host $pp

        # Now parse the packageParameters using regular expression
        if ($pp) {
            Write-Verbose "Manage Parameters here"
        }
        else {
            Write-Verbose "No Package Parameters Passed in, defaults will apply"
        }

        Push-location "$psscriptroot\..\Content"

        if (test-path $DeployFile) {
            Write-Host "Chocolatey will now start the deployment file $DeployFile"
            . "$DeployFile" -env $env
        }
        else{
        	Throw "Deployment has been unable to trigger deployment file $DeployFile"
        }
    }
    catch{
        Throw $_
    }
    finally{
        Pop-Location
    }