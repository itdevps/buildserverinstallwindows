
#Pre-requisites to be able to install a module from psgallery using artifactory proxy.
register-psrepository -Name "nuget@artifactory" -SourceLocation "https://repo.int.garda.com/artifactory/api/nuget/nuget" -InstallationPolicy "Trusted"

#Installating Nuget Package Manager
Install-PackageProvider -Name NuGet -Force -Confirm:$false

#Installing provider DockerMsftProvider for the docker installation command.
Install-Module -Name DockerMsftProvider -Force

#Pakage source for install package command
register-packagesource -ProviderName Nuget -location "https://repo.int.garda.com/artifactory/api/nuget/nuget" -name "nuget@artifactory" -trusted

#TLS 1.2 compliant
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

#Installing Docker Engine and Daemon enterprise
#Manually: https://docs.docker.com/engine/install/binaries/
Install-Package Docker -ProviderName DockerMsftProvider -Force -Verbose

#Installing chocolatey
powershell -Command [System.Net.WebRequest]::DefaultWebProxy.Credentials = [System.Net.CredentialCache]::DefaultCredentials;Set-ExecutionPolicy Bypass -Scope Process -Force;[System.Net.WebRequest]::DefaultWebProxy.Credentials = [System.Net.CredentialCache]::DefaultCredentials;[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072;iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

#installing Git
choco install git --y --confirm:$false -force

$RestartNeeded=(Install-WindowsFeature Containers).RestartNeeded

#Reboot Computer
if ($RestartNeeded -eq "yes"){
    Restart-Computer
}